import numpy as np
from math import exp, sqrt

from ase.calculators.morse import MorsePotential as BaseMorse


def create_mask(atoms):
    """
    Code modified from
    J. A. Garrido Torres, E. Garijo del Rio, V. Streibel,
    T. S. Choski, J. J. Mortensen, A. Urban, M. Bajdich,
    F. Abild-Pedersen, K. W. Jacobsen, and T. Bligaard (submitted).
    """

    constraints = atoms.constraints
    mask_constraints = np.ones_like(atoms.positions, dtype=bool)
    for c in constraints:

        if hasattr(c, 'index'):
            mask_constraints[c.index] = False

        if hasattr(c, 'a') and hasattr(c, 'mask'):
            mask_constraints[c.a] = ~c.mask

        return mask_constraints.reshape(-1)


def get_numeric_hessian(atoms, step):
    """
    Get the Hessian of constrained atoms using
    finite differences
    """
    mask_constraints = create_mask(atoms)
    p0 = atoms.get_positions().flatten()

    indices = np.array([i for i in range(3*len(atoms))])[mask_constraints]

    # Compute all the gradients
    terms = {}
    for i in indices:
        # +step displacement
        p1 = p0.copy()
        p1[i] += step
        atoms.set_positions(p1.reshape(-1, 3))
        gplus = -atoms.get_forces().flatten()

        # -step displacement
        p1 = p0.copy()
        p1[i] -= step
        atoms.set_positions(p1.reshape(-1, 3))
        gminus = -atoms.get_forces().flatten()

        terms[i] = (gplus - gminus) / (4 * step)

    # Compute Hessian
    def get_entry(i1, i2, terms):
        return terms[i1][i2] + terms[i2][i1]

    H = np.block([[get_entry(i, j, terms)
                  for i in indices]
                  for j in indices])
    # Symmetrize
    return 0.5 * (H + H.T)


class MorsePotential(BaseMorse):
    """
    Morse potential.
    Extension of ASE's MorsePotential to include
    the exact Hessian.
    """

    implemented_properties = ['energy', 'forces', 'hessian']
    default_parameters = {'epsilon': 1.0,
                          'rho0': 6.0,
                          'r0':   1.0}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=['positions', 'numbers', 'cell',
                                  'pbc', 'charges', 'magmoms']):

        super().calculate(atoms=atoms, properties=properties,
                          system_changes=system_changes)

        epsilon = self.parameters.epsilon
        rho0 = self.parameters.rho0
        r0 = self.parameters.r0

        positions = self.atoms.get_positions()
        N = len(positions)
        hessian = np.zeros((3*N, 3*N))
        preF = 2 * epsilon * rho0 / r0

        eye = np.eye(3)

        for i1, p1 in enumerate(positions):
            for i2, p2 in enumerate(positions):

                if i2 == i1:
                    continue

                diff = p1-p2
                r = sqrt(np.sum(diff ** 2))
                expf = exp(rho0 * (1.0 - r / r0))
                outer = np.outer(diff, diff)

                h = expf * (expf - 1) * (eye / r - outer / r ** 3)
                h -= rho0 / r0 * expf * (2 * expf - 1) * outer / r ** 2
                h *= preF

                hessian[i1*3:(i1+1)*3, i1*3:(i1+1)*3] -= h
                hessian[i1*3:(i1+1)*3, i2*3:(i2+1)*3] += h

        self.results['hessian'] = hessian

    def get_hessian(self, atoms=None):
        return self.get_property('hessian', atoms)
