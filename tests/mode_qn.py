from trts.trust_region import Quasi_TRTS
from ase.build import fcc100, add_adsorbate
from ase import Atoms
from ase.constraints import FixAtoms
from ase.optimize import BFGS
from trts.utils import MorsePotential

import numpy as np


atoms = fcc100('Al', (4, 4, 2), vacuum=6.0)
adsorbate = Atoms('Al')

add_adsorbate(atoms, adsorbate, position='hollow', height=2.0)

x = atoms.get_positions()
x[-1, :2] += 2.864
atoms.set_positions(x)

c = FixAtoms(mask=[a.tag >= 1 for a in atoms])
atoms.set_constraint(c)
atoms.set_pbc((True, True, False))

x = atoms.get_positions()
x[-1, 0] += 0.2
atoms.set_positions(x)
atoms.set_calculator(MorsePotential(r0=2.8, rho0=1.7*2.8, eps=0.002))

opt = BFGS(atoms)
opt.run(0.01)

x = atoms.get_positions()
directions = [np.array([0.1, 0.05]),
              np.array([0.05, 0.1]),
              np.array([-0.1, 0.05]),
              np.array([0.05, -0.1])]

for i, d in enumerate(directions):
    x0 = x.copy()
    x0[-1, :2] += d
    atoms.set_positions(x0)

    op = Quasi_TRTS(atoms, mode=(x0 - x).flatten(), delta=0.3, 
                       trajectory='mode_{}.traj'.format(i))
    op.run(0.05, 250)

