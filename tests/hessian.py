from trts.utils import get_numeric_hessian, MorsePotential, create_mask

from ase.build import fcc100, add_adsorbate
from ase.constraints import FixAtoms, FixedPlane
from ase import Atoms

import numpy as np

atoms = fcc100('Au', (2, 2, 2), vacuum=6.)
au = Atoms('Au', positions=np.zeros(3).reshape(1, 3))

add_adsorbate(atoms, au, position='hollow', height=1.6)

c = FixAtoms(mask=[a.tag >= 1 for a in atoms])
c2 = FixedPlane(len(atoms)-1, np.array([0, 0, 1]))

atoms.set_constraint([c, c2])

atoms.set_calculator(MorsePotential())
atoms.get_potential_energy()

print(get_numeric_hessian(atoms, 0.5))
H = atoms.get_calculator().get_hessian()
mask = create_mask(atoms)

H = np.array([v[mask] for v in H[mask]])
print(H)
