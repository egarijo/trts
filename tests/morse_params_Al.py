from ase import Atoms
from ase.calculators.emt import EMT

import numpy as np
import matplotlib.pyplot as plt

e = np.zeros(60)
r = np.linspace(1.6, 5.8, 60)

for i, d in enumerate(r):
    Al2 = Atoms('Al2', positions=np.array([[0, 0, 0], [0, 0, d]]),
                cell=(4, 4, 40))
    Al2.set_calculator(EMT())
    e[i] = Al2.get_potential_energy()

r0 = r[np.argmin(e)]
print(r0)
epsilon = e[-1]-min(e)
print(epsilon)

i = np.argmin(e)

ke = (e[i+1]+e[i-1] - 2 * e[i]) / (r[i]-r[i-1])**2
rho0 = np.sqrt(ke/(2*epsilon))
print(rho0)

plt.plot(r, e)
plt.show()
