from trts.trust_region.quasi_nw import Quasi_TRTS, Bofill
from ase.build import fcc100, add_adsorbate
from ase import Atoms
from ase.constraints import FixAtoms
from ase.optimize import BFGS
from trts.utils import MorsePotential

atoms = fcc100('Al', (4, 4, 2), vacuum=6.0)
adsorbate = Atoms('Al')

add_adsorbate(atoms, adsorbate, position='hollow', height=2.0)

x = atoms.get_positions()
x[-1, :2] += 2.864
atoms.set_positions(x)

c = FixAtoms(mask=[a.tag >= 1 for a in atoms])
atoms.set_constraint(c)
atoms.set_pbc((True, True, False))


x = atoms.get_positions()
x[-1, 0] += 0.2
atoms.set_positions(x)
atoms.set_calculator(MorsePotential(r0=2.8, rho0=1.7*2.8, eps=0.002))

opt = BFGS(atoms)
opt.run(0.01)

x = atoms.get_positions()
x[-1, 0] += 0.1
x[-1, 1] += 0.05
atoms.set_positions(x)

op = Quasi_TRTS(atoms, delta=0.005, alpha=1./40., hessian_update=Bofill,
                trajectory='ts.traj')
op.run(0.05, 250)
