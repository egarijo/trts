from trts.ai_nr_ts import AIDTRTS
from trts.trust_region import Quasi_TRTS

from ase.optimize.activelearning.gp.calculator import GPCalculator

from ase.io import read
from ase.build import fcc100, add_adsorbate
from ase.calculators.emt import EMT
from ase.constraints import FixAtoms, FixedPlane
from ase import Atoms

import numpy as np

flag = True

atoms = fcc100('Au', (3, 3, 2), vacuum=6.)

au = Atoms('Au', positions=[[0, 0, 0]])

add_adsorbate(atoms, au, height=2.0, position='hollow')

c = FixAtoms(mask=[a.tag > 0 for a in atoms])
c2 = FixedPlane(len(atoms)-1, np.array([0, 0, 1]))
atoms.set_constraint([c, c2])

atoms.set_calculator(EMT())

if flag:
    atoms0 = atoms.copy()
    atoms0.set_calculator(EMT())
    atoms0.get_potential_energy()
    atoms0.get_forces()
    training_set = [atoms0]

else:
    training_set = read('training.traj@:')


p0 = atoms.get_positions()
p0[-1, :2] += 0.3
atoms.set_positions(p0)


model_calculator = GPCalculator(params={'weight': 2.0, 'scale': 0.6},
                                calculate_uncertainty=True,
                                params_to_update={},
                                mask_constraints=True,
                                update_prior_strategy='maximum')

opt = AIDTRTS(atoms, model_calculator=model_calculator,
              model_optimizer=Quasi_TRTS, trajectory='aits.traj',
              trainingset=training_set, use_previous_observations=True,
              kappa=0.9)

for converged in opt.irun(fmax=0.05):
    if converged:
        break
    input()
