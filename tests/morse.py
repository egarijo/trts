from trts.utils import MorsePotential
from ase.collections import g2

import numpy as np

atoms = g2['O2']
atoms.set_calculator(MorsePotential())
N = len(atoms)

print(atoms._calc.get_hessian(atoms))
print('\n')

H = np.zeros((3*N, 3*N))
f0 = atoms.get_forces().flatten()
x0 = atoms.get_positions().flatten()
h = 0.0000001

for i in range(3*N):
    x1 = x0.copy()
    x1[i] += h

    atoms.set_positions(x1.reshape(-1, 3))

    f1 = atoms.get_forces().flatten()
    H[i] = -(f1-f0)/h

H[np.abs(H) < 1e-4] = 0.0
print(H)
