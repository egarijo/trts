from trts.trust_region.nw_raphson import NewtonRaphson
from trts.trust_region.quasi_nw import Quasi_TRTS

__all__ = ['NewtonRaphson', 'Quasi_TRTS']
