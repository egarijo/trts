import numpy as np
from ase.optimize.optimize import Optimizer

from trts.utils import create_mask
from trts.trust_region.trust_region import TrustRegion


class NewtonRaphson(Optimizer):

    def __init__(self, atoms, logfile='-', trajectory=None, master=None,
                 mode=None, rmin=0.70, rgood=0.85, gamma=1.5, delta=0.2,
                 eps=1e-5):

        # Init the optimization routines
        super().__init__(atoms, restart=None, logfile=logfile,
                         trajectory=trajectory, master=master)

        # Constraints (we only consider unconstrained d.o.f.)
        self.mask = create_mask(atoms)

        # Mask the constrained coordinates of the mode
        if mode is not None:
            mode = mode[self.mask]

        # Parameters for the trust region
        self.trustr = TrustRegion(mode=mode,
                                  delta=delta,
                                  rmin=rmin,
                                  rgood=rgood,
                                  gamma=gamma,
                                  eps=eps)

    def f(self, x):
        """
        Function (PES) whose 1st order saddle points
        we want to find as a function of the unconstrained
        d.o.f x
        """
        pos = np.zeros(3 * len(self.atoms))
        pos[self.mask] = x

        self.atoms.set_positions(pos.reshape(-1, 3))
        return self.atoms.get_potential_energy()

    def g(self, x):
        """
        Gradient of the PES as a function of the unconstrained
        d.o.f. x
        """
        pos = np.zeros(3 * len(self.atoms))
        pos[self.mask] = x
        self.atoms.set_positions(pos.reshape(-1, 3))

        g = -self.atoms.get_forces().flatten()
        return g[self.mask]

    def H(self, x):
        """
        Hessian of the PES as a function of the uncostrained
        d.o.f. x
        """
        pos = np.zeros(3 * len(self.atoms))
        pos[self.mask] = x
        self.atoms.set_positions(pos.reshape(-1, 3))

        H = self.atoms._calc.get_hessian(self.atoms)
        H = np.vstack([h[self.mask] for h in H[self.mask]])
        return H

    def step(self):

        x0 = self.atoms.get_positions().flatten()[self.mask]
        g0 = self.g(x0)
        H0 = self.H(x0)

        x, delta = self.trustr.step(self.f, g0, H0, x0)

        x1 = np.zeros(3 * len(self.atoms))
        x1[self.mask] = x
        self.atoms.set_positions(x1.reshape(-1, 3))

        self.delta = delta
