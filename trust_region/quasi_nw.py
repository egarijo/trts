from trts.trust_region.nw_raphson import NewtonRaphson

import numpy as np
from trts.utils import get_numeric_hessian


def SR1(B0, x0, g0, x1, g1):
    s = x1 - x0
    y = g1 - g0

    v = y-np.matmul(B0, s)

    return np.outer(v, v) / np.dot(v, s)


def PSB(B0, x0, g0, x1, g1):
    s = x1 - x0
    y = g1 - g0

    v = y - np.matmul(B0, s)

    Dh_plus = (np.outer(v, s) + np.outer(s, v)) / np.dot(s, s)
    Dh_minus = np.dot(s, v) * np.outer(s, s) / np.dot(s, s)**2

    return Dh_plus - Dh_minus


def Bofill(B0, x0, g0, x1, g1):
    s = x1 - x0
    y = g1 - g0

    v = y - np.matmul(B0, s)
    a = np.dot(v, s)**2 / (np.dot(v, v) * np.dot(s, s))

    sr1 = np.outer(v, v) / np.dot(v, s)
    psb_plus = (np.outer(v, s) + np.outer(s, v)) / np.dot(s, s)
    psb_minus = np.dot(s, v) * np.outer(s, s) / np.dot(s, s)**2

    return a * sr1 + (1 - a) * (psb_plus - psb_minus)


class Quasi_TRTS(NewtonRaphson):

    def __init__(self, atoms, alpha=0.2, hessian_update=Bofill,
                 num_hessian=True, **kwargs):

        super().__init__(atoms, **kwargs)

        self.Hk = None
        self.alpha = alpha
        self.hessian_update = hessian_update
        self.num_hessian = num_hessian

    def init_Hessian(self):
        if self.num_hessian:
            self.Hk = get_numeric_hessian(self.atoms, 0.01)
        else:
            np.random.seed(42)
            d = 0.2 * np.random.randn(int(np.sum(self.mask))) + 1
            self.Hk = self.alpha * np.diag(d)
        self.xk = self.atoms.get_positions().flatten()[self.mask]
        self.gk = self.g(self.xk)

    def update_Hessian(self):
        x1 = self.atoms.get_positions().flatten()[self.mask]
        g1 = self.g(x1)
        self.Hk += self.hessian_update(self.Hk, self.xk, self.gk, x1, g1)
        self.xk = x1
        self.gk = g1

    def H(self, x):
        if self.Hk is None:
            self.init_Hessian()
        return self.Hk

    def step(self):
        super().step()
        self.update_Hessian()
