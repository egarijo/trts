import numpy as np


class QuadraticForm():
    def __init__(self, x0, y0, g0, H0, eps):
        self.x0 = x0
        self.y0 = y0
        self.g0 = g0
        self.H0 = H0
        self.eps = eps

    def quadratic(self, x1):
        p = x1 - self.x0
        return self.y0 + np.dot(self.g0, p) + np.dot(p, np.dot(self.H0, p)) / 2

    def eigh(self):
        w, v = np.linalg.eigh(self.H0)
        # Clear out symmetries
        # eps is the threshold for the symmetries
        nonzero = np.abs(w) > self.eps
        self.w = w[nonzero]
        self.v = v.T[nonzero]

    def get_critical_point(self):
        g_proj = np.matmul(self.v, self.g0)
        coef = - g_proj / self.w
        return np.matmul(coef, self.v)

    def shift_Hessian(self, h1, h2):
        """
        After reference:
        J. Nichols, H. Taylor, P. Schmidt and J. Simons
        Walking on potential energy surfaces,
        J. Chem. Phys. 92, 340 (1990)
        """

        # Choose lambda and alpha (rescaling)
        # There are still some cases we havent accounted for
        # TODO: what if the eigenvalues have different sign?
        if h1 > 0:
            if h2/2 > h1:
                a = 1.0
                lam = 0.5 * (h1 + h2/2)
            else:
                a = (h2-h1)/h2
                lam = 0.5 * (h1 + (h1+h2)/2)

        elif h2 < 0:
            if h1/2 < h2:
                a = 1.0
                lam = 0.5 * (h2 + h1/2)
            else:
                a = (h1-h2)/h1
                lam = 0.5 * (h2 + (h2+h1) * 0.5)

        else:
            a = 1.0
            lam = 0.0

        assert h1 - lam < 0
        assert h2 - lam > 0
        assert h1 * (1 - a/2) - lam < 0
        assert h2 * (1 - a/2) - lam > 0

        # Actually shift Hessian
        eye = np.eye(len(self.H0))
        self.H0 = (self.H0 - lam * eye) / a
        self.w = (self.w - lam) / a


class TrustRegion():

    def __init__(self, mode=None, delta=1.,
                 rmin=0.25, rgood=0.5, gamma=2.0, eps=1e-4):

        self.v0 = mode

        self.rmin = rmin
        self.rgood = rgood
        self.gamma = gamma
        self.delta = delta
        self.eps = eps

    def step(self, f, g, H, x):

        self.quad = QuadraticForm(x, f(x), g, H, self.eps)

        # Find the mode to follow
        self.mode_following()

        self.accepted = False
        x = self.trial_step(f, x)

        while not self.accepted:
            x = self.trial_step(f, x)

        # Update the direction mode
        if self.v0 is not None:
            self.v0 = self.v1.copy()

        return x, self.delta

    def trial_step(self, f, x):
        """
        Borrowed (and modified) from:
        M. J. Kochenderfer and T. A. Wheeler
        Algorithms for Optimization
        MIT Press (2019)
        """

        y = f(x)
        x1, y1 = self.solve_trust_region_subproblem(x, y)
        r = (y - f(x1)) / (y-y1)
        if (r >= self.rmin) and (r <= self.rgood):
            x, y = x1, y1
            self.accepted = True

        elif (2 - self.rgood < r) and (r < 2 - self.rmin):
            x, y = x1, y1
            self.accepted = True

        elif (self.rgood <= r) and (r <= 2 - self.rgood):
            x, y = x1, y1
            self.delta *= self.gamma
            self.accepted = True

        elif (r < self.rmin) or (r > 2-self.rmin):
            self.delta /= self.gamma
            self.accepted = False

        return x

    def solve_trust_region_subproblem(self, x0, y0):
        """
        After reference:
        J. Nichols, H. Taylor, P. Schmidt and J. Simons
        Walking on potential energy surfaces,
        J. Chem. Phys. 92, 340 (1990)
        """

        # Critical point of the quadratic mode
        p = self.quad.get_critical_point()

        if np.sum(p**2) > self.delta**2:
            # The saddle point is outside the trust region
            # Redefine p, keep the direction,
            # resize it to be length delta
            p = self.delta*p/np.sqrt(np.sum(p**2))

        x1 = x0 + p
        y1 = self.quad.quadratic(x1)
        return x1, y1

    def mode_following(self):

        # Compute eigenvalues, eigenvectors and their shifts
        # Find eigenvalues and eigenvectors:
        self.quad.eigh()

        # Choose which mode to follow
        if self.v0 is None:
            h1, h2 = softest_mode_following(self.quad)
        else:
            h1, h2, self.v1 = previous_mode_following(self.quad, self.v0)
        self.quad.shift_Hessian(h1, h2)


def previous_mode_following(quad, v0):
    """
    Find the eigenvector v0 that overlaps the most with v_old
    and follow it
    """
    # Get index of the eigenvalue with the largest overlap in abs
    proj = np.abs(np.matmul(quad.v, v0))
    i0 = np.argmax(proj)

    # If the largest overlap is with the smallest eigenvalue
    if i0 == 0:
        h1, h2 = softest_mode_following(quad)
        return h1, h2, quad.v[0]

    # Otherwise the mode we want to follow has h0 eigenvalue.
    h0 = quad.w[i0]
    h2 = quad.w[0]

    # Make h0 the smallest eigenvalue
    if h0 == h2:
        err_msg = ("Equal smallest eigenvalues of the Hessian is"
                   " not implemented")
        raise NotImplementedError(err_msg)

    if h2 > 0:  # All eig are positive
        # Shrink the component along v0
        h1 = h2 / 3

    elif h2 < 0 and h0 > 0:
        # First shift all the eigenvalues to make them positive
        lam = - 2 * h2

        # Change eignevalues and Hessian
        h0 += lam
        h2 += lam
        quad.w += lam
        quad.H0 += lam * np.eye(len(quad.H0))
        # Now shrink the component along v0

        h1 = h2 / 3

    else:  # h2 and h0 are negative
        # h1/2 < h2
        h1 = 3*h2

    # Change eigenvalue  accordingly
    quad.w[i0] = h1

    # Change Hessian and gradient accordingly
    quad.g0[i0] *= np.sqrt(h1/h0)
    quad.H0 += (h1-h0) * np.outer(quad.v[i0], quad.v[i0])

    return h1, h2, quad.v[i0].copy()


def softest_mode_following(quad):
    h1, h2 = quad.w[:2]  # smallest eigenvalues

    if h1 == h2:
        err_msg = 'Equal smallest eigenvalyes of Hessian is not implemented'
        raise NotImplementedError(err_msg)

    return h1, h2
