import numpy as np

from ase.optimize import GPMin
from ase.calculators.emt import EMT

from ase import Atoms
from ase.build import fcc100, add_adsorbate

from ase.constraints import FixAtoms, FixedPlane

from ase.io import read

atoms = fcc100('Au', (3,3,2), vacuum=6.)

au = Atoms('Au', positions = [[0,0,0]])

add_adsorbate(atoms, au, height=2.0, position='bridge')

p = atoms.get_positions()
p0 = p[-1,:2].copy()
p[-1,1] -= 0.9
p[-1,0] -= 0.3
atoms.set_positions(p)

c = FixAtoms(mask = [a.tag > 0 for a in atoms])
c2 = FixedPlane(len(atoms)-1, np.array([0,0,1]))
atoms.set_constraint([c,c2])

atoms.set_calculator(EMT())

op = GPMin(atoms, trajectory='training.traj')
op.run(0.01)

images = read('training.traj@:')
pos = np.array([im.get_positions()[-1,:2]-p0 for im in images])
print(pos)
np.savez('training.npz', pos)

