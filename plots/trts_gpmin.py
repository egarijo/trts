from ase.optimize.activelearning.gp.calculator import GPCalculator

from trts.trust_region import Quasi_TRTS

from ase.io import read
from ase import Atoms
from ase.build import fcc100, add_adsorbate
from ase.constraints import FixAtoms, FixedPlane

import numpy as np

import sys

theta = float(sys.argv[1])
A = float(sys.argv[2])

atoms = read('training.traj')
p = atoms.get_positions()
p[-1, :2] += A*np.array([np.cos(theta * np.pi), np.sin(theta * np.pi)])
atoms.set_positions(p)

c = FixAtoms(mask=[a.tag > 0 for a in atoms])
c2 = FixedPlane(len(atoms)-1, np.array([0, 0, 1]))
atoms.set_constraint([c, c2])


images = read('training.traj@:')

calc = GPCalculator(train_images=images,
                    params={'weight': 2.0,
                            'scale': 1.4},
                    noise=0.001,
                    calculate_uncertainty=False,
                    params_to_update={},
                    mask_constraints=True,
                    update_prior_strategy='maximum')
atoms.set_calculator(calc)

ts = Quasi_TRTS(atoms, trajectory='quasi_trts.traj', alpha=1./40., delta=0.02)
ts.run(0.0001, steps=200)


####
atoms = fcc100('Au', (3, 3, 2), vacuum=6.)

au = Atoms('Au', positions=[[0, 0, 0]])

add_adsorbate(atoms, au, height=2.0, position='bridge')

p = atoms.get_positions()
p0 = p[-1, :2].copy()


probed = read('quasi_trts.traj@:')
x = np.array([a.get_positions()[-1, :2]-p0 for a in probed])

np.savez('trajectory.npz', x)
