import numpy as np
from numpy.linalg import eigh
from itertools import product

def get_UV(g, H, delta):
    w, v = eigh(H)
    v = v.T

    # Get right direction
    X = np.dot(g,v[0])/np.abs(w[0]) * v[0] 
    X -= np.dot(g,v[1])/np.abs(w[1]) * v[1]

    if np.dot(X,X) > delta**2:
        X *= delta/np.sqrt(np.dot(X,X))
    return X[0], X[1]


def get_gH(atoms, h):
    E = atoms.get_potential_energy()

    def get_gradient(atoms):
        f = atoms.get_forces()
        return -f[-1,:2]
    
    g0 = get_gradient(atoms)
    H = np.zeros((2,2))

    # Translate x:
    pos = atoms.get_positions()
    pos[-1,0] += h
    atoms.set_positions(pos)

    g1 = get_gradient(atoms)

    H[0,:] = (g1-g0)/h
    H[1,0] = H[0,1]

    # Translate y
    pos = atoms.get_positions()
    pos[-1,1] += h
    atoms.set_positions(pos)

    g1 = get_gradient(atoms)
    H[1,1] = (g1[1]-g0[1])/h

    return E, g0, H


def get_phases(atoms, h, delta):
    E, g, H = get_gH(atoms, h)
    U, V = get_UV(g, H, delta)

    return U, V


def get_EUV_matrices(atoms, X, Y, h, delta):
    p0 = atoms.get_positions()
    s = X.shape
    U = np.zeros(s)
    V = np.zeros(s)
    E = np.zeros(s)

    for i, j in product(range(s[0]), range(s[1])):
        p = p0.copy()
        p[-1,0] += X[i,j]
        p[-1,1] += Y[i,j]

        atoms.set_positions(p)
        E[i,j] = atoms.get_potential_energy()
        U[i,j], V[i,j] = get_phases(atoms, h, delta)

    return E, U, V


