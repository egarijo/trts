import numpy as np
from ase.optimize.activelearning.gp.gp import GaussianProcess
from ase.optimize.activelearning.gp.kernel import SquaredExponential
from ase.optimize.activelearning.gp.prior import ConstantPrior

import matplotlib.pyplot as plt


_default_params = dict(
        p = 0.01,
        f0 = 0.5,
        g = 0.4,
        alpha = 0.5*np.pi ,

        scale=1.,
        noise = 1.0e-3,
        prior_c = 0.5,

        h=1e-4)


def get_H_determinant(p, f0, g, alpha, scale, noise, prior_c, h):
    gp_params = {'scale': scale, 'weight':1.}

    # Inputs
    x0 = np.array([0., 0])
    x1 = p* np.array([1., 0])

    X = np.array([x0, x1])

    # Targets
    e0 = 0.
    e1 = f0

    g0 = np.array([0., 0.])
    g1 = np.array([np.cos(alpha), np.sin(alpha)])


    y0 = np.insert(g0, 0, e0)
    y1 = np.insert(g1, 0, e1)

    Y = np.array([y0, y1])


    # Gaussian process

    kernel = SquaredExponential()
    prior = ConstantPrior(prior_c)

    gp = GaussianProcess(kernel=kernel, prior=prior)
    gp.set_hyperparams(gp_params, noise)

    gp.train(X, Y)

    # Get the Hessian

    def fdiff(i,j):
        x = x0.copy()
        x[0] += i*h
        x[1] += j*h
        return gp.predict(x)[0][0]

    H = np.zeros((2,2))
    H[0,0] = fdiff(2, 0) - 2 * fdiff(0, 0) + fdiff(-2, 0)
    H[1,1] = fdiff(0, 2) - 2 * fdiff(0, 0) + fdiff(0, -2)
    H[0,1] = fdiff(1, 1) - fdiff(1,-1) - fdiff(-1,1) + fdiff(-1,-1)
    H[1,0] = H[0,1]

    H /= 4*h**2

    return np.linalg.det(H)


alpha = np.linspace(0., 2*np.pi, 150)
det = []

for a in alpha:
    kwargs = _default_params.copy()
    kwargs['alpha'] = a
    kwargs['p'] = 0.01
    det.append(get_H_determinant(**kwargs))

plt.polar(alpha, det, label='E_p=0.5')


det = []
for a in alpha:
    kwargs = _default_params.copy()
    kwargs['prior_c'] = 1.
    kwargs['p']=0.01
    kwargs['alpha'] = a
    det.append(get_H_determinant(**kwargs))
plt.polar(alpha, det, label='E_p=200.0')


plt.polar(alpha, np.zeros(150), 'k--')

plt.legend()
plt.show()

