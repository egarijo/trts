import numpy as np
import matplotlib.pyplot as plt


fig, axs = plt.subplots(1,3, sharey=True)


### Minimum

ax0, ax1, ax2 = axs[0], axs[1], axs[2]

npzfile = np.load('min.npz')

(X, Y, E, U, V) = (npzfile[f'arr_{i}'] for i in range(5))


ax0.streamplot(X, Y, U, V, density=1., color='k')
ax0.set_title('Minimum')

filled_marker_style = dict(marker='o', 
        markeredgecolor='black', 
        markerfacecolor='white',
        markersize=7)

ax0.plot([0.], [0.], 'o', fillstyle = 'full', **filled_marker_style)


### Saddle

npzfile = np.load('saddle.npz')

(X, Y, E, U, V) = (npzfile[f'arr_{i}'] for i in range(5))

ax1.streamplot(X, Y, U, V, density=1., color='k')
ax1.set_title('Saddle point')

filled_marker_style = dict(marker='o', markeredgecolor='white', markerfacecolor='black', markersize=7)

ax1.plot([0.], [0.], 'o', fillstyle = 'full', **filled_marker_style)

## Maximum

npzfile = np.load('max.npz')

(X, Y, E, U, V) = (npzfile[f'arr_{i}'] for i in range(5))

ax2.streamplot(X, Y, U, V, density=1., color='k')
ax2.set_title('Maximum')



filled_marker_style = dict(marker='o',
        markeredgecolor='black',
        markerfacecolor='white',
        markersize=7)

ax2.plot([0.], [0.], 'o', fillstyle = 'full', **filled_marker_style)


for ax in axs:
    ax.set_xlim(-0.1, 0.1)
    ax.set_ylim(-0.1, 0.1)

plt.show()
