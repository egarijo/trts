import numpy as np
import matplotlib.pyplot as plt


fig, (axs0, axs1) = plt.subplots(2, 2, sharey=True)


### EMT

ax0, ax1 = axs0[0], axs0[1]

npzfile = np.load('emt.npz')

(X, Y, E, U, V) = (npzfile[f'arr_{i}'] for i in range(5))

cs0 = ax0.contourf(X, Y, E)
ax0.contour(cs0, colors='k')

ax0.set_title('PES')
ax1.streamplot(X, Y, U, V, density=1.5, color='k')
ax1.set_title('Phase plot')

ax0.set_ylim(-2,2)
ax1.set_ylim(-2,2)


### GPMin

ax0, ax1 = axs1[0], axs1[1]

npzfile = np.load('gpmin.npz')

(X, Y, E, U, V) = (npzfile[f'arr_{i}'] for i in range(5))

cs0 = ax0.contourf(X, Y, E)
ax0.contour(cs0, colors='k')

ax1.streamplot(X, Y, U, V, density=1.5, color='k')

# training set
npzfile = np.load('training.npz')
x = npzfile['arr_0']


ax0.plot(x[:,0], x[:,1], 'o', color='tab:orange')
ax1.plot(x[:,0], x[:,1], 'o', color='tab:orange')

ax0.set_ylim(-2,2)
ax1.set_ylim(-2,2)

plt.show()
