import numpy as np
import matplotlib.pyplot as plt


fig, (axs0, axs1) = plt.subplots(2, 2, sharey=True, sharex=True)

# EMT
ax0 = axs0[0]

npzfile = np.load('emt.npz')
(X, Y, E) = (npzfile[f'arr_{i}'] for i in range(3))

cs0 = ax0.contourf(X, Y, E)
ax0.contour(cs0, colors='k')

ax0.set_title('PES')

# Other axis

for ax in [axs0[1], axs1[0], axs1[1]]:
    npzfile = np.load('gpmin.npz')
    (X, Y, E) = (npzfile[f'arr_{i}'] for i in range(3))

    cs = ax.contourf(X, Y, E)
    ax.contour(cs, colors='k')

    npzfile = np.load('training.npz')
    x = npzfile['arr_0']

    ax.plot(x[:,0], x[:,1], 'o', color='tab:orange')

axs0[1].plot([x[-1,0]], [x[-1,1]], 'x', color='white')
axs0[1].set_title('Relaxation')

# Trajectories
files = ['trajectory.npz', 'trajectory1.npz']

for f, ax in zip(files, axs1):
    npzfile = np.load(f)
    x = npzfile['arr_0']

    ax.plot(x[:,0], x[:,1], 'o', color='tab:red')
    ax.plot([x[-1,0]], [x[-1,1]], 'x', color='white')
    ax.set_title('Saddle')


plt.savefig("project_update.pdf")
