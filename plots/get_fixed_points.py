from ase import Atoms
from ase.calculators.emt import EMT
from ase.build import fcc100, add_adsorbate

from trts.plots.find_directions import get_EUV_matrices

import numpy as np


def get_neighborhood(position, filename):
    atoms = fcc100('Au', (3,3,2), vacuum=6.)

    au = Atoms('Au', positions = [[0,0,0]])

    add_adsorbate(atoms, au, height=2.0, position=position)

    atoms.set_calculator(EMT())


    x = np.linspace(-0.1, 0.1, 20)
    y = np.linspace(-0.1, 0.1, 20)

    X, Y = np.meshgrid(x, y)

    h = 0.001
    delta = 0.2

    E, U, V = get_EUV_matrices(atoms, X, Y, h, delta)

    np.savez(filename, X, Y, E, U, V)


if __name__ == "__main__":
    get_neighborhood('bridge', 'saddle.npz')
    get_neighborhood('ontop', 'max.npz')
    get_neighborhood('hollow', 'min.npz')
