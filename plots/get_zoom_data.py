from ase.io import read
from ase import Atoms
from ase.build import fcc100, add_adsorbate
from ase.optimize.activelearning.gp.calculator import GPCalculator

from trts.plots.find_directions import get_EUV_matrices

import numpy as np

## Atoms object
atoms = fcc100('Au', (3,3,2), vacuum=6.)

au = Atoms('Au', positions = [[0,0,0]])

add_adsorbate(atoms, au, height=2.0, position='bridge')



images = read('training.traj@:')
atoms = images[-1]
p0 = atoms.get_positions()[-1,:2]


calc = GPCalculator(train_images=images,
                       params={'weight': 2.0,
                               'scale': 0.4},
                       calculate_uncertainty=False,
                       params_to_update={},
                       mask_constraints=True,
                       update_prior_strategy='maximum')
atoms.set_calculator(calc)



x = np.linspace(-0.5, 0.5, 20)
y = np.linspace(-0.5, 0.5, 20)

X, Y = np.meshgrid(x, y)

h = 0.001
delta = 0.2

E, U, V = get_EUV_matrices(atoms, X, Y, h, delta)

np.savez('zoom.npz', X, Y, E, U, V)

