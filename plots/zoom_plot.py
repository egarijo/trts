import numpy as np
import matplotlib.pyplot as plt

from ase.io import read

fig, axs0 = plt.subplots(1, 2, sharey=True)


### EMT

ax0, ax1 = axs0[0], axs0[1]

npzfile = np.load('zoom.npz')

(X, Y, E, U, V) = (npzfile[f'arr_{i}'] for i in range(5))

cs0 = ax0.contourf(X, Y, E, 35)
ax0.contour(cs0, colors='k')

ax0.set_title('PES')
ax1.streamplot(X, Y, U, V, density=1.5, color='k')
ax1.set_title('Phase plot')

ax0.set_ylim(-0.5, 0.5)
ax1.set_ylim(-0.5, 0.5)

images = read('training.traj@:')
p0 = images[-1].get_positions()[-1,:2]
x = np.array([im.get_positions()[-1,:2] - p0 for im in images])

ax0.plot(x[:,0], x[:,1], 'o', color='tab:orange')
ax0.plot([x[-1,0]], [x[-1,1]], 'x', color='white')
# Mark saddle point
#filled_marker_style = dict(marker='o', markeredgecolor='white', markerfacecolor='black', markersize=7)

#ax1.plot([0.], [0.], 'o', fillstyle = 'full', **filled_marker_style)


plt.show()
