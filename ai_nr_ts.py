from ase.optimize.optimize import Optimizer
from ase.optimize.activelearning.trainingset import TrainingSet

from trts.trust_region import Quasi_TRTS


class AIDTRTS(Optimizer):
    def __init__(self, atoms, model_calculator, model_optimizer,
                 restart=None, trajectory=None, logfile='-',
                 fit_to='calc', trainingset=[], kappa=None,
                 use_previous_observations=False,
                 **kwargs):

        super().__init__(atoms, restart=restart,
                         trajectory=trajectory, logfile=logfile, **kwargs)
        self.model_calculator = model_calculator
        self.model_optimizer = model_optimizer

        self.fit_to = fit_to

        # Deal with training set here
        self.train = TrainingSet(trainingset, use_previous_observations)
        self.attach(self.train, atoms=atoms, method='ts')

        # Early stopping parameter
        self.kappa = kappa
        if self.kappa is not None:
            if not self.model_calculator.calculate_uncertainty:
                err_msg = ('model calculator must calculate '
                           'uncertainty if kappa is not None')
                raise AttributeError(err_msg)

    def step(self):

        # 1. Update training set
        train_images = self.train.load_set()
        # Remove constraints
        for img in train_images:
            if self.fit_to == 'calc':
                img.constraints = []
            elif self.fit_to == 'constraints':
                img.set_constraint(self.constraints)

        self.model_calculator.update_train_data(train_images=train_images,
                                                test_images=[self.atoms.copy()]
                                                )

        # 2. Saddle point searches
        ml_atoms = self.atoms.copy()
        self.find_surrogate_saddle(ml_atoms)

        # 3. Evaluate the target function
        self.atoms.set_positions(ml_atoms.get_positions())
        self.atoms.get_potential_energy(force_consistent=self.force_consistent)
        self.atoms.get_forces()

        # 4. Post processing
        self.function_calls = len(train_images) + 1
        self.force_calls = self.function_calls

    def find_surrogate_saddle(self, atoms):
        # Set model calculator
        atoms.set_calculator(self.model_calculator)

        # Optimizer
        opt = self.model_optimizer(atoms, logfile=None)
        for _ in opt.irun(fmax=self.fmax * 0.01):

            # Early stopping
            if self.kappa is not None:
                u = self.model_calculator.results['uncertainty']
                max_u = self.model_calculator.kernel.weight

                if u >= self.kappa * max_u:
                    print('early stopping')
                    break


class MLModeFollower(AIDTRTS):
    def __init__(self, atoms, model_calculator, mode, numeric_hessian=True,
                 **kwargs):

        model_optimizer = Quasi_TRTS
        super().__init__(atoms, model_calculator, model_optimizer, **kwargs)

        self.mode = mode  # right now this is not being used.
        self.numH = numeric_hessian  # right now not being used.

    def find_surrogate_saddle(self, atoms):
        # Set model calculator
        atoms.set_calculator(self.model_calculator)

        # Optimizer
        opt = self.model_optimizer(atoms, logfile=None,
                                   mode=self.mode, num_hessian=self.numH)

        for _ in opt.irun(fmax=self.fmax * 0.01):

            # Early stopping
            if self.kappa is not None:
                u = self.model_calculator.results['uncertainty']
                max_u = self.model_calculator.kernel.weight

                if u >= self.kappa * max_u:
                    print('early stopping')
                    break
